import * as React from 'react';

import styled from 'styled-components';

import EventBox from './EventBox';

import { IColoredEvent } from './store';

interface IProps {
  events: IColoredEvent[];
}

const DateLine = styled.div`
  margin-bottom: 6px;
  font-size: 20px;
  line-height: 1.35;
  font-weight: 600;
`;

class EventList extends React.Component<IProps> {
  private groupByDate() {
    let prevDate: string | undefined;

    interface EventGroup {
      events: IColoredEvent[];
      date: string;
    }

    let eventGroups: EventGroup[] = [];
    let events: IColoredEvent[] = [];

    this.props.events.forEach(
      event => {
        const date = event.date;

        if (!prevDate) {
          prevDate = date;
        } else if (date !== prevDate) {
          eventGroups.push({
            date: prevDate,
            events,
          });
          events = [];
          prevDate = date;
        }
        events.push(event);
      }
    );
    if (prevDate && events.length) {
      eventGroups.push({
        date: prevDate,
        events,
      });
    }
    return eventGroups;
  }

  public renderDate(date: string) {
    const months = [
      'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
    ];
    const d = new Date(date);
    return d.getDate() + ' ' + months[d.getMonth()];
  }

  public render() {
    return (
      <div>
        {
          this.groupByDate().map(
            group => (
              <section key={group.date}>
                <DateLine>{this.renderDate(group.date)}</DateLine>
                {
                  group.events.map(
                    (event, i) => (
                      <EventBox key={i} {...event} />
                    )
                  )
                }
              </section>
            )
          )
        }
      </div>
    );
  }
}

export default EventList;
