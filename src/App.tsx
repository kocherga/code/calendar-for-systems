import * as React from 'react';
import { inject, observer, Provider } from 'mobx-react';

import styled from 'styled-components';

import Calendar  from 'react-calendar';

import { Store, toMoscowDate } from './store';

import EventList from './EventList';

const TildaColumn = styled.div`
  @media screen and (max-width: 960px) {
    margin-bottom: 50px;
  }
`;

const TildaColumn4 = TildaColumn.extend`
  max-width: 400px;
  @media screen and (max-width: 960px) {
    padding-left: 20px;
    padding-right: 20px;
    width: 100%;
  }
  @media screen and (max-width: 390px) {
    width: 100%;
    max-width: none;
    margin-left: auto;
    margin-right: auto;
    padding: 0;
  }
`;

const TildaColumn8 = TildaColumn.extend`
  flex: 1;
  padding-left: 40px;
  @media screen and (max-width: 960px) {
    padding-left: 40px;
    padding-right: 40px;
  }
  @media screen and (max-width: 390px) {
    padding-left: 20px;
    padding-right: 20px;
  }
`;

const Layout = styled.div`
  max-width: 1200px;
  padding: 0;
  width: 100%;
  margin: 0 auto;

  * {
    box-sizing: border-box;
  }

  display: flex;
  flex-direction: row;
  @media screen and (max-width: 960px) {
    flex-direction: column;
    max-width: 600px;
  }

  font-family: 'Mediator', sans-serif;

  .react-calendar {
    width: auto;
  }

  .react-calendar__tile--past {
    background-color: #f0f0f0;
  }

  .react-calendar__tile--active {
    background-color: #4799eb !important;
  }

  .react-calendar__tile--active:hover {
    background-color: #a3ccf5 !important;
  }

  .react-calendar__navigation__label {
    font-size: 16px;
  }
  @media screen and (max-width: 390px) {
    .react-calendar__navigation__label {
      font-size: 14px;
    }
  }

  .react-calendar__month-view__weekdays__weekday abbr {
    text-decoration: none;
  }
`;

const Circle = styled.span`
  height: 6px;
  width: 6px;
  margin-right: 2px;
  border-radius: 50%;
  display:inline-block;
`;

interface ICommonProps {
  store?: Store;
}

interface IDateProps extends ICommonProps {
  date: Date;
}

@inject('store')
@observer
class TileContent extends React.Component<IDateProps> {
  public render() {
    const events = this.props.store!.date2events[toMoscowDate(this.props.date)];
    return (
      <div>
        {
          (events && events.length)
          ? events.map(
            (event, i) => (
              <Circle key={i} style={{backgroundColor: event.color}} />
            )
          )
          : <Circle style={{backgroundColor: 'transparent'}} />
        }
      </div>
    );
  }
}

interface IDateProps extends ICommonProps {
  date: Date;
}

@inject('store')
@observer
class DateEventList extends React.Component<IDateProps> {
  public render() {
    // const events = this.props.store!.date2events[toMoscowDate(this.props.date)] || [];
    const events = this.props.store!.nextEvents(this.props.date);
    return <EventList events={events} />
  }
}

interface IProps {
  config: any;
};

interface IState {
  store: Store;
  date: Date;
};

@observer
class App extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      store: new Store(props.config),
      date: new Date(),
    };
    this.state.store.fetchData().then(
      () => {
        const events = this.state.store.allEvents;
        if (!events) {
          return;
        }
        const now = new Date().getTime();
        this.setState({
          date: new Date(Math.min(...events.filter(e => e.ts > now).map(e => e.ts)))
        });
      }
    );
  }

  private tileFunction({ date, view }: { date: Date, view: string }) {
    if (view !== 'month') {
      return null;
    }
    return <TileContent date={date} />
  }

  private tileClassFunction({ date, view }: { date: Date, view: string }) {
    const now = new Date();
    if (view === 'year') {
      if (
        date.getFullYear() > now.getFullYear()
        || (date.getFullYear() === now.getFullYear() && date.getMonth() >= now.getMonth())
      ) {
        return '';
      }
    }

    if (view === 'month') {
        if (date.getTime() >= now.getTime() - 86400 * 1000) {
          return '';
        }
    }
    return 'react-calendar__tile--past';
  }

  private renderCalendar() {
    return (
      <Calendar
        locale="ru-RU"
        calendarType="ISO 8601"
        minDetail="year"
        tileContent={this.tileFunction}
        tileClassName={this.tileClassFunction}
        onChange={this.onChange}
        onClickMonth={this.onClickMonth}
        onActiveDateChange={this.onActiveDateChange}
        value={this.state.date}
      />
    );
  }

  private renderContent() {
    return (
      <div className="t-descr">
        <DateEventList date={this.state.date} />
      </div>
    );
  }

  public render() {
    return (
      <Provider store={this.state.store}>
        <Layout>
          <TildaColumn4>
            {this.renderCalendar()}
          </TildaColumn4>
          <TildaColumn8>
            {this.renderContent()}
          </TildaColumn8>
        </Layout>
      </Provider>
    );
  }

  private onChange = (date: Date) => {
    this.setState({ date });
  }

  private onActiveDateChange = ({ activeStartDate, view } : { activeStartDate: Date, view: string }) => {
    let date = new Date();
    if (activeStartDate.getUTCFullYear() !== date.getUTCFullYear() || activeStartDate.getMonth() !== date.getMonth()) {
      date = activeStartDate;
    }

    this.setState({ date });
  }

  private onClickMonth = (date: Date) => {
    this.setState({ date });
  }
}

export default App;
