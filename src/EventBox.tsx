import * as React from 'react';

import { IColoredEvent } from './store';

import styled from 'styled-components';

const Box = styled.section`
  margin-bottom: 18px;
  line-height: 1.55;
  @media screen and (max-width: 960px) {
    font-size: 0.9em;
  }

  p {
    margin-top: 0px;
    margin-bottom: 4px;
  }
`;

const BoxCalendarLink = styled.div`
  float: right;
  font-size: 0.8em;
  @media screen and (max-width: 960px) {
    float: none;
    display: block;
  }
`;


class EventBox extends React.Component<IColoredEvent> {
  public render() {
    const addLink = `https://calendar.google.com/calendar/r/eventedit/copy/${this.props.tmeid}`;
    return (
      <Box>
        <span style={{color: this.props.color}}>
          <time>{this.props.time}</time>—<time>{this.props.endTime}</time>
        </span>
        {' '}
        {
          this.props.link ?
          <a href={this.props.link}>{this.props.title}</a>
          : this.props.title
        }
        <BoxCalendarLink>
          <a href={addLink} target="_blank" rel="noopener noreferrer">добавить в календарь</a>
        </BoxCalendarLink>
        {
          this.props.description && <p>{this.props.description}</p>
        }
      </Box>
    );
  }
}

export default EventBox;
