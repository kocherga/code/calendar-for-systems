import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';

// import registerServiceWorker from './registerServiceWorker';

let script: any = document.getElementById('calendar-widget');

if (script) {
  const config = eval(script.text);
  const root = document.createElement('div');
  script.parentNode.insertBefore(root, script);

  ReactDOM.render(
    <App config={config} />,
    root
  );
// registerServiceWorker();
}
