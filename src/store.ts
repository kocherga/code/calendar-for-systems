import { observable, computed } from 'mobx';

interface IEvent {
  id: string;
  title: string;
  link?: string;
  description?: string;
  tmeid: string;
  calendar_id: string;
  date: string;
  time: string;
  endTime: string;
  ts: number;
}

export interface IColoredEvent extends IEvent {
  color: string;
}

interface ICalendar {
  id: string;
  color: string;
  events?: IEvent[];
}

export const toMoscowDate = (date: Date) => {
  return new Date(
    date.getTime()
    + 3 * 3600 * 1000 // 3 hours in milliseconds
    + 3000 // for some weird borderline cases of leap seconds, etc. - we call to toMoscowDate(UTC midnight) in some places and don't want to miss a midnight by a few seconds
  ).toISOString().substring(0, 10);
};

export class Store {
  @observable
  data: ICalendar[] = [];

  constructor(config: any) {
    this.data = config;
  }

  public async fetchData() {
    const apiKey = 'AIzaSyA98K6tICQDAYdZnaqmFqkfQr2TP0LENgE';

    const timeDelta = 12 * 30 * 86400 * 1000;
    const timeMin = new Date((new Date().getTime() - timeDelta)).toISOString();
    const timeMax = new Date((new Date().getTime() + timeDelta)).toISOString();

    for (const calendar of this.data) {
        const response = await fetch(
          `https://www.googleapis.com/calendar/v3/calendars/${calendar.id}/events?key=${apiKey}&singleEvents=true&timeMin=${timeMin}&timeMax=${timeMax}&maxResults=2500`
        );
        const json = await response.json();

        calendar.events = json.items.map(
          (item: any) => {
            let link;
            let description;
            if (item.description) {
              const match = item.description.match(/^(https?\S+)\s*\n?([^]*)$/m);
              if (match) {
                link = match[1];
                description = match[2];
              }
            }

            const tmeid = item.htmlLink.match(/eid=(\S+)/)[1];

            // We use timezone-specific date and time encoded as strings,
            // relying on the fact that google calendar always returns the date in its timezone.
            const date = item.start.dateTime.substring(0, 10);
            const time = item.start.dateTime.substring(11, 16);
            const endTime = item.end.dateTime.substring(11, 16);
            return {
              id: item.id,
              calendar_id: calendar.id,
              tmeid,
              link, description,
              title: item.summary,
              date, time, endTime,
              ts: new Date(item.start.dateTime).getTime(),
            };
          }
        );
      }
  }

  @computed get
  date2events() {
    const result: { [key: string]: IColoredEvent[] } = {};

    this.data.forEach(
      cal => (cal.events || []).forEach(
        event => {
          const date = event.date;
          if (!result[date]) {
            result[date] = [];
          }
          result[date].push({
            color: cal.color,
            ...event
          });
        }
      )
    );
    return result;
  }

  @computed get
  allEvents() {
    const events: IColoredEvent[] = [];
    this.data.slice().forEach(
      cal => (cal.events || []).forEach(
        event => events.push({ color: cal.color, ...event })
      )
    );
    return events.sort(
      (e1, e2) => {
        if (e1.ts < e2.ts) {
          return -1;
        }
        if (e1.ts > e2.ts) {
          return 1;
        }
        return 0;
      }
    );
  }

  nextEvents(fromDate: Date) {
    const limit = 5;
    const date = new Date(fromDate);
    date.setHours(0, 0, 0);
    const events = this.allEvents.filter(
      event => event.ts >= date.getTime()
    ).slice(0, limit);

    return events;
  }
}
