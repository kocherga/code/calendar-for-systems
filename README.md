# Виджет календаря

В этом репозитории - исходный код виджета для сайта ШСМ (и, возможно, Кочерги).

Технологии: React, MobX, styled-components.

# Как выкладывать

1. `yarn build`
2. Залить `build/static/css/main.[код].css` и `build/static/js/main.[код].js` куда-нибудь. Например, на S3.

# Как использовать

## Создание календарей

Заведите Google-календари на каждый вид события.

Сделайте их публично доступными в настройках календаря.

Сохраните Calendar ID (есть в настройках), они вам понадобятся.

## Настройка сайта

Вставить следующий код на страницу:

```html
<link rel="stylesheet" href="https://s3.eu-central-1.amazonaws.com/berekuk-public/calendar-for-systems/main.css"/>
<script id="calendar-widget">
    [
        {
            id: 'SOME_CALENDAR_ID@group.calendar.google.com',
            color: 'red',
        },
        {
            id: 'ANOTHER_CALENDAR_ID@group.calendar.google.com',
            color: 'green',
        },
    ]
</script>
<script src="https://s3.eu-central-1.amazonaws.com/berekuk-public/calendar-for-systems/main.js"></script>
```

JSON в коде большого `<script>` исправить:

* добавить столько календарей, сколько необходимо
* настроить цвета по своему вкусу (справка: [цвета HTML](https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML))
